/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 ** modified by: Felipe Luis
 **/

#include "springmass.h"

int main(int argc, char** argv)
{
  SpringMass springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 0.7; // acrescentado também o valor ao 'stiffnes'

  Mass m1(Vector2(-0.2,0.6), Vector2(), mass, radius) ; // inciando o vetor y com valores diferentes
  Mass m2(Vector2(+0.2,0.1), Vector2(), mass, radius) ; // tentando coorigir erro de impressão

  /* INCOMPLETE: TYPE YOUR CODE HERE 
     1. Adicione as duas massas instanciadas acima (2 linhas).
     2. Adicione uma mola com as duas massas (indexadas por 0 e 1),
        e com o parametro de comprimento em repouso inicializado acima.
	(1 linha).
   */

  Spring spring(&m1, &m2, naturalLength, stiffness);

  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addSpring(&spring);
  
  const double dt = 1.0/30 ;
  for (int i = 0 ; i < 30 ; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
  }

  return 0 ;
}
